#include <filesystem>
#include <fstream>

#include <catch2/catch.hpp>

#include "../src/brokerconfigfile.h"



void RemoveDataFile(std::string path)
{
     if (std::filesystem::exists(path))
    {
        std::filesystem::remove(path);
    }
}

void WriteDataToFile(std::string path, std::string contents)
{
    RemoveDataFile(path);
    std::fstream file;
    file.open(path, std::fstream::out);
    file << contents;
    file.close();
}


TEST_CASE("File Initialised with no params", "[BrokerConfigFile]")
{
    BrokerConfigFile config_file;
    REQUIRE("" == config_file.GetPath());
    REQUIRE("" == config_file.GetProfileName());
    REQUIRE("" == config_file.GetHost());
    REQUIRE(0 == config_file.GetPort());
    REQUIRE("" == config_file.GetUsername());
    REQUIRE("" == config_file.GetPassword());
    REQUIRE(true == config_file.GetJson().empty());
}


TEST_CASE("File Initialised with only path", "[BrokerConfigFile]")
{
    std::string path = "/tmp/config.json";
    BrokerConfigFile config_file(path);
    REQUIRE(path == config_file.GetPath());
    REQUIRE("" == config_file.GetProfileName());
    REQUIRE("" == config_file.GetHost());
    REQUIRE(0 == config_file.GetPort());
    REQUIRE("" == config_file.GetUsername());
    REQUIRE("" == config_file.GetPassword());
    REQUIRE(true == config_file.GetJson().empty());
}

TEST_CASE("File Initialised with no path", "[BrokerConfigFile]")
{
    BrokerConfigFile config_file;

    SECTION("No need to clean up profile name to filename")
    {
        config_file.SetProfileName("TestProfile");

        REQUIRE(BrokerConfigFile::CONFIG_DIR_ + "/" + "TestProfile" + BrokerConfigFile::CONFIG_EXT_ == config_file.GetPath());
        REQUIRE("TestProfile" == config_file.GetProfileName());
    }

    SECTION("Space in profile name")
    {
        config_file.SetProfileName("Test Profile ");

        REQUIRE(BrokerConfigFile::CONFIG_DIR_ + "/" + "TestProfile" + BrokerConfigFile::CONFIG_EXT_ == config_file.GetPath());
        REQUIRE("Test Profile " == config_file.GetProfileName());
    }

    SECTION("Other characters in profile name")
    {
        config_file.SetProfileName("Test/Profile.,()");

        REQUIRE(BrokerConfigFile::CONFIG_DIR_ + "/" + "TestProfile" + BrokerConfigFile::CONFIG_EXT_ == config_file.GetPath());
        REQUIRE("Test/Profile.,()" == config_file.GetProfileName());
    }
}



TEST_CASE("Parse Basic Config File", "[BrokerConfigFile]")
{
    std::string path = "/tmp/basic_config.json";
    std::string config_contents = R"({"ProfileName":"Test 123","Host":"test.mosquitto.org","Port":1883,"ClientID":"SH_Client","Credentials":{"Username":"user","Password":"password"}})";

    WriteDataToFile(path, config_contents);

    BrokerConfigFile config_file(path);
    REQUIRE(true == config_file.Read());

    REQUIRE("Test 123" == config_file.GetProfileName());
    REQUIRE("test.mosquitto.org" == config_file.GetHost());
    REQUIRE(1883 == config_file.GetPort());
    REQUIRE("SH_Client" == config_file.GetClientID());
    REQUIRE("user" == config_file.GetUsername());
    REQUIRE("password" == config_file.GetPassword());

    RemoveDataFile(path);
}

TEST_CASE("Parse Basic Config File With Missing Item", "[BrokerConfigFile]")
{
    std::string path = "/tmp/basic_config.json";
    std::string config_contents = R"({"ProfileName":"Test 123","Port":1883,"ClientID":"SH_Client","Credentials":{"Username":"user","Password":"password"}})";

    WriteDataToFile(path, config_contents);

    BrokerConfigFile config_file(path);
    REQUIRE(false == config_file.Read());

    REQUIRE("" == config_file.GetProfileName());
    REQUIRE("" == config_file.GetHost());
    REQUIRE(0 == config_file.GetPort());
    REQUIRE("" == config_file.GetClientID());
    REQUIRE("" == config_file.GetUsername());
    REQUIRE("" == config_file.GetPassword());

    RemoveDataFile(path);
}

TEST_CASE("Parse Basic Config File of Invalid JSON", "[BrokerConfigFile]")
{
    std::string path = "/tmp/basic_config.json";
    std::string config_contents = "This is not valid JSON";

    WriteDataToFile(path, config_contents);

    BrokerConfigFile config_file(path);
    REQUIRE(false == config_file.Read());

    REQUIRE("" == config_file.GetProfileName());
    REQUIRE("" == config_file.GetHost());
    REQUIRE(0 == config_file.GetPort());
    REQUIRE("" == config_file.GetClientID());
    REQUIRE("" == config_file.GetUsername());
    REQUIRE("" == config_file.GetPassword());

    RemoveDataFile(path);
}


TEST_CASE("Write Basic Config File", "[BrokerConfigFile]")
{
    std::string path = "/tmp/basic_config_write.json";
    std::string config_contents = R"({"ProfileName":"Test 123","Host":"test.mosquitto.org","Port":1883,"ClientID":"SH_Client","Credentials":{"Username":"user","Password":"password"}})";

    WriteDataToFile(path, config_contents);

    BrokerConfigFile config_file(path);
    REQUIRE(true == config_file.Read());

    // Remove Data File so we can overwrite it
    RemoveDataFile(path);
    REQUIRE(false == std::filesystem::exists(path));

    // Write the data back out
    config_file.Write();

    // Check the data
    BrokerConfigFile config_file_new(path);
    REQUIRE(true == config_file_new.Read());
    REQUIRE(config_file_new.GetJson() == config_file.GetJson());
}