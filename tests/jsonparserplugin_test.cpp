#include <QString>
#include <iostream>

#include <catch2/catch.hpp>
#include <nlohmann/json.hpp>

#include "../src/plugins/json/jsonparserplugin.h"

using json = nlohmann::json;


TEST_CASE("Empty payload", "[JsonParsePlugin]")
{
    JsonParserPlugin plugin;
    QString input{""};
    QString output{""};

    bool parse_result = plugin.ParseAndFormat(input, output);

    REQUIRE(parse_result == false);
    REQUIRE(input == "");
    REQUIRE(output == input);
}

TEST_CASE("Invalid JSON payload", "[JsonParsePlugin]")
{
    JsonParserPlugin plugin;
    QString input = R"(["test" : 123])";
    QString input_orig = input;
    QString output{""};

    bool parse_result = plugin.ParseAndFormat(input, output);

    REQUIRE(parse_result == false);
    REQUIRE(input == input_orig);
    REQUIRE(output == input);
}


TEST_CASE("Basic JSON payload", "[JsonParsePlugin]")
{
    JsonParserPlugin plugin;
    QString input = R"({"Hello":"World","ET":{"Phone":"Home"}})";
    QString input_orig = input;
    QString output{""};

    bool parse_result = plugin.ParseAndFormat(input, output);

    REQUIRE(parse_result == true);
    REQUIRE(input == input_orig);

    json in = json::parse(input.toStdString());
    json out = json::parse(output.toStdString());
    REQUIRE(in == out);
}