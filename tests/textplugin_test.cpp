#include <QString>
#include <iostream>

#include <catch2/catch.hpp>

#include "../src/plugins/text/textplugin.h"


TEST_CASE("Empty message payload", "[TextPlugin]")
{
    TextPlugin plugin;
    QString input{""};
    QString output{""};

    bool parse_result = plugin.ParseAndFormat(input, output);

    // What goes in, comes out, parse_result always true
    REQUIRE(parse_result == true);
    REQUIRE(input == "");
    REQUIRE(output == input);
}

TEST_CASE("Bad JSON payload", "[TextPlugin]")
{
    TextPlugin plugin;
    QString input = R"(["test" : 123])";
    QString input_orig = input;
    QString output{""};

    bool parse_result = plugin.ParseAndFormat(input, output);

    // What goes in, comes out, parse_result always true
    REQUIRE(parse_result == true);
    REQUIRE(input == input_orig);
    REQUIRE(output == input);
}

TEST_CASE("Good JSON payload", "[TextPlugin]")
{
    TextPlugin plugin;
    QString input = R"({"Hello":"World","ET":{"Phone":"Home"}})";
    QString input_orig = input;
    QString output{""};

    bool parse_result = plugin.ParseAndFormat(input, output);

    // What goes in, comes out, parse_result always true
    REQUIRE(parse_result == true);
    REQUIRE(input == input_orig);
}

TEST_CASE("Example Protobuf payload", "[TextPlugin]")
{
    TextPlugin plugin;
    QString input = R"(CgRTaW9uEMDEBw==)";
    QString input_orig = input;
    QString output{""};

    bool parse_result = plugin.ParseAndFormat(input, output);

    // What goes in, comes out, parse_result always true
    REQUIRE(parse_result == true);
    REQUIRE(input == input_orig);
    REQUIRE(output == input);
}
