[![pipeline status](https://gitlab.com/sion.w.hughes/datganmqtt/badges/master/pipeline.svg)](https://gitlab.com/sion.w.hughes/datganmqtt/-/commits/master)
[![license](https://img.shields.io/badge/license-MIT-blue)](https://choosealicense.com/licenses/mit/)

# Datgan MQTT

## Introduction

Datgan is an MQTT client with a GUI. Connect to a broker and subscribe, receive and publish messages.

![Datgan's Main Screen](/images/main.png)

![Datgan's Broker Config Screen](/images/brokerconfig.png)

![Datgan's Publish Screen](/images/publish.png)

Datgan has a plugin interface allowing the writing of plugins to process MQTT payloads into a readable format.

## Building

To build, you'll need to install the following packages (tested on Ubuntu 20.04)

 - g++
 - cmake
 - qt5-default
 - libprotobuf-dev
 - protobuf-compiler
 - libmosquitto-dev
 - doxygen
 - graphviz

 Then, make a out of source build dir:

 ```
 $ mkdir build
 $ cd build
 ```

 Configure:

 ```
 $ cmake ..
 ```

 Build main app:
```
 $ cmake --build .
```

Build plugins:
```
$ cmake --build . --target plugins
```


## Tests

To run tests, confgure as above and then run CTest:

```
cmake ..
ctest
```

## Packaging

To create distributable package (currently only .deb), configure and build app and plugins. Then run CPack:

```
$ cpack
```