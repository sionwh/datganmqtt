##
## Application Packaging
##

set(INSTALL_PATH /opt/datgan/)

include(InstallRequiredSystemLibraries)
install(TARGETS ${CMAKE_PROJECT_NAME}
    COMPONENT application
    RUNTIME DESTINATION "${INSTALL_PATH}"
    LIBRARY DESTINATION "${INSTALL_PATH}"
    DESTINATION "${INSTALL_PATH}"
)
install(TARGETS json-parser-plugin
    COMPONENT plugin
    RUNTIME DESTINATION "${INSTALL_PATH}plugins"
    LIBRARY DESTINATION "${INSTALL_PATH}plugins"
    DESTINATION "${INSTALL_PATH}plugins"
)

# Subsitute in the version number and project name
configure_file("package/Datgan.desktop.in" "package/Datgan.desktop")

INSTALL(FILES ${CMAKE_BINARY_DIR}/package/Datgan.desktop DESTINATION share/applications)
INSTALL(FILES resources/icons/megaphone.png DESTINATION ${INSTALL_PATH})

# autogenerate dependency information
set (CPACK_DEBIAN_PACKAGE_SHLIBDEPS ON)

set(CPACK_GENERATOR "DEB")
set(CPACK_DEBIAN_PACKAGE_MAINTAINER "Siôn Hughes")
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "MQTT Client with a GUI")
set(CPACK_DEBIAN_PACKAGE_DESCRIPTION "MQTT Client with a GUI. Publish messages to a broker, and received message on subscriptions. Plugin interface included to write custom message parsing plugins.")
set(CPACK_DEBIAN_PACKAGE_SECTION "Utilities")
set(CPACK_DEBIAN_PACKAGE_HOMEPAGE "https://gitlab.com/sion.w.hughes/datganmqtt/")
include(CPack)