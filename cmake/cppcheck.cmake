##
## Taken from https://gitlab.cern.ch/corryvreckan/corryvreckan/-/blob/6a6d06ea01f167bc1c8da2ef871d9974b345447b/cmake/clang-cpp-checks.cmake
##

find_program(CPPCHECK "cppcheck")
if(CPPCHECK)
    # Set export commands on
    set (CMAKE_EXPORT_COMPILE_COMMANDS ON)

    add_custom_target(
        cppcheck
        COMMAND ${CPPCHECK}
        --enable=all
        --project=${CMAKE_BINARY_DIR}/compile_commands.json
        --std=c++17
        --verbose
        --quiet
        --xml-version=2
        --language=c++
        --library=qt
        --suppressions-list=${CMAKE_SOURCE_DIR}/suppressions.txt
        --output-file=${CMAKE_BINARY_DIR}/cppcheck_results.xml
        ${CHECK_CXX_SOURCE_FILES}
        COMMENT "Generate cppcheck report for the project"
    )

    find_program(CPPCHECK_HTML "cppcheck-htmlreport")
    if(CPPCHECK_HTML)
        add_custom_target(
            cppcheck-html
            COMMAND ${CPPCHECK_HTML}
            --title=${CMAKE_PROJECT_NAME}
            --file=${CMAKE_BINARY_DIR}/cppcheck_results.xml
            --report-dir=${CMAKE_BINARY_DIR}/cppcheck_results
            --source-dir=${CMAKE_SOURCE_DIR}
            COMMENT "Convert cppcheck report to HTML output"
        )
        ADD_DEPENDENCIES(cppcheck-html cppcheck)
    endif()
endif()