/** @file
 * @brief Implementation of the ReceivedMessageDelegate class which is responsible for setting values in an entry
 * in a MessageList instance which shows details of an MQTT message
 *
 * Copyright 2020 Siôn Hughes
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, 
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or 
 * substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 **/
#include <iostream>
#include <QDateTime>

#include "receivedmessagedelegate.h"


ReceivedMessageDelegate::ReceivedMessageDelegate(QObject *parent) :
	QStyledItemDelegate(parent)
{

}

void ReceivedMessageDelegate::paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    QStyledItemDelegate::paint(painter,option,index);
    
    painter->save();

    QFont font;
    QFont SubFont;
    //font.setPixelSize(font.weight()+);
    font.setBold(true);
    SubFont.setWeight(SubFont.weight()-2);
    QFontMetrics fm(font);
    
    QRect headerRect = option.rect;
    QRect subheaderRect = option.rect;

    headerRect.setTop(headerRect.top()+5);
    headerRect.setBottom(headerRect.top()+fm.height());
    
    subheaderRect.setTop(headerRect.bottom()+2);
    
    painter->setFont(font);
    painter->drawText(headerRect,index.data(Qt::UserRole).toString());
   
    painter->setFont(SubFont);
    QString details;

    // Set Timestamp and it's date conversion
    QDateTime date;
    date.setTime_t(index.data(Qt::UserRole+2).toInt());
    details = "Timestamp: " + index.data(Qt::UserRole+2).toString();
    details += " (" + date.toString("hh:mm:ss - yyyy/MM/dd") + ")";

    // Set QoS, tabbed out from the Timestamp
    details += "\t";
    details += "QoS: " + index.data(Qt::UserRole+3).toString();
    painter->drawText(subheaderRect.left(),subheaderRect.top()+17,details);

    painter->restore();
    
}

QSize ReceivedMessageDelegate::sizeHint(const QStyleOptionViewItem & option, const QModelIndex & index ) const
{

    QSize result = QStyledItemDelegate::sizeHint(option, index);
    result.setHeight(result.height()*3);
    return result;
}
