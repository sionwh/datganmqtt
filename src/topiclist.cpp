/** @file
 * @brief Implementation of the TopicList class which is responsible for showing a list of subscribed topics
 *
 * Copyright 2020 Siôn Hughes
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 **/
#include <QStandardItemModel>
#include <QString>
#include <iostream>

#include "topiclist.h"
#include "subscriptiondelegate.h"

TopicList::TopicList(QWidget *parent):
	QListView(parent)
{
    auto *delegate = new SubscriptionDelegate(this);

    setModel(new QStandardItemModel(this));
	setItemDelegate(delegate);
}

void TopicList::ClearSubscriptions()
{
    static_cast<QStandardItemModel *>(model())->clear();
}

void TopicList::AddSubscription(const std::string& topic)
{
    auto *item = new QStandardItem();

	item->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
    item->setData(QString::fromStdString(topic),Qt::UserRole);
    item->setData(int32_t{0},Qt::UserRole+1);

	static_cast<QStandardItemModel *>(model())->appendRow(item);
}