/** @file
 * @brief Header of the MainWindow class which is the main UI for the Datgan Application
 *
 * Copyright 2020 Siôn Hughes
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, 
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or 
 * substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 **/
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMutex>
#include <QPluginLoader>
#include <map>
#include <QColor>

#include "mqtt.h"
#include "brokerconfigfile.h"
#include "plugins/ipayloadparserplugin.h"


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void showEvent(QShowEvent *event);

signals:
    void MessageReceivedSignal(QString topic, QString payload, std::chrono::seconds timestamp, int qos);
    void ConnectionStateChangedSignal(bool state);

private slots:
    void on_lv_SentMessages_clicked(const QModelIndex &index);

private slots:
    void on_pb_Cancel_clicked();

private slots:
    void on_cb_Connection_activated(int index);
    void on_pb_Connect_clicked();

    void on_pb_Subscribe_clicked();

    void on_pb_Disconnect_clicked();

    void on_pb_ConnectionSettings_clicked();

    void on_listView_clicked(const QModelIndex &index);

    void MessageReceivedSlot(QString topic, QString payload, std::chrono::seconds timestamp, int qos);

    void on_lv_subscriptions_clicked(const QModelIndex &index);

    void on_pb_RemoveSubscription_clicked();

    void on_btn_night_clicked();

    void on_cb_Plugins_currentIndexChanged(const QString &arg1);

    void ConnectionStateChangedSlot(bool state);

    void on_pb_ClearMessages_clicked();

    void on_pb_Publish_clicked();

    void on_pb_PauseScroll_clicked();

private:
    Ui::MainWindow *ui;
    void PopulateConnections();
    static void MessageReceivedCallback(std::string topic, std::string payload, std::chrono::seconds timestamp, int qos);
    void LoadPlugins();
    void PopulatePlugins();
    void FormatAndShowMessagePayload(QString payload);
    static void ConnectCallback();
    static void DisconnectCallback();
    void ClearReceivedMessages();
    void ClearPublishedMessages();
    void ClearSubscriptions();
    void DisconnectCleanup();

    Mqtt mqtt;

    QString newconnect_{"New Connection"};
    const std::string plugins_dir_{"/opt/datgan/plugins"};
    const std::string plugins_ext_{".so"};
    std::map<QString, IPayloadParserplugin*> message_format_plugins_;
    const QColor message_parse_fail_colour_{255, 204, 204};
    const QColor message_parse_success_colour_{214, 245, 214};
    QString currentSelectedPlugin;

    BrokerConfigFile *connection_config_;
    std::map<std::string, std::string> connection_configs_;
    int_fast64_t messages_received_{0};

    bool night_mode_{false}; 

    bool pause_message_scroll_{true}; //!< Flag to indicate whether to scroll received message list on receivng a new message
};
#endif // MAINWINDOW_H
