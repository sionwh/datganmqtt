/** @file
 * @brief Header for the BrokerConfigFile class which is responsible for reading and writing connection
 * profiles to and from config files on the system
 *
 * Copyright 2020 Siôn Hughes
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 **/

#ifndef BROKERCONFIGFILE_H
#define BROKERCONFIGFILE_H

#include <string>

#include <nlohmann/json.hpp>

using json = nlohmann::json;

class BrokerConfigFile
{
public:
    BrokerConfigFile() = default;
    BrokerConfigFile(std::string path);

    bool Read();
    void Write();
    std::string GetPath();
    std::string GetProfileName();
    std::string GetHost();
    int_fast16_t GetPort();
    std::string GetClientID();
    std::string GetUsername();
    std::string GetPassword();
    bool GetTlsEnable();
    std::string GetCAPath();
    std::string GetCertPath();
    std::string GetKeyPath();
    json GetJson();
    void SetPath(std::string path);
    std::string SetProfileName(std::string name);
    void SetHost(std::string host);
    void SetPort(int_fast16_t port);
    void SetClientID(std::string clientid);
    void SetUsername(std::string username);
    void SetPassword(std::string password);
    void SetTlsEnable(bool tls_enable);
    void SetCAPath(std::string ca_path);
    void SetCertPath(std::string cert_path);
    void SetKeyPath(std::string key_path);

    static const std::string CONFIG_EXT_;
    static const std::string CONFIG_DIR_;

private:
    void CreateConfigDir();

    std::string path_{""};
    std::string profile_name_{""};
    std::string host_{""};
    int_fast16_t port_{0};
    std::string clientid_{""};
    std::string username_{""};
    std::string password_{""};
    bool tls_enable_{false};
    std::string ca_path_{""};
    std::string cert_path_{""};
    std::string key_path_{""};
    json json_contents_;



};

#endif // BROKERCONFIGFILE_H
