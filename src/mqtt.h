/** @file
 * @brief Header of the Mqtt class which is responsible for MQTT communications between the application and a broker
 * Provides methods for connecting, subscribing and publishing
 *
 * Copyright 2020 Siôn Hughes
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 **/
#ifndef MQTT_H
#define MQTT_H
#include<QString>

#include<functional>
#include<map>
#include<chrono>

#include<mosquitto.h>

class Mqtt
{
public:
    Mqtt() = default;
    void Initialise(const QString& broker_addr, const int_fast16_t broker_port, const QString& client_id);
    void Connect(std::function<void(void)> connect_callback, std::function<void(void)> disconnect_callback, const std::string& username="", const std::string& password="", const std::string& cafile="", const std::string& key="", const std::string& cert="");
    void Disconnect();
    void Destroy();
    static void MessageReceivedCallback(struct mosquitto *connection, void *userdata, const struct mosquitto_message *message);
    static void ConnectCallback(struct mosquitto *connection, void *userdata, int conn_code);
    static void DisconnectCallback(struct mosquitto *connection, void *userdata, int conn_code);

    void Subscribe(QString topic, std::function<void(const std::string &, const std::string &, const std::chrono::seconds &, const int &)> callback, int qos=2);
    void Unsubscribe(QString topic);

    void Publish(QString topic, QString payload, int qos, bool retained);

    static bool DoesTopicMatchSub(const QString& subscription, const QString& topic);

private:
    void RouteReceivedMessage(QString topic, QString payload, std::chrono::seconds timestamp, int qos);
    void PassConnectionEvent();
    void PassDisconnectionEvent();

    struct mosquitto *mosquitto_conn_ = NULL;

    std::map<QString,std::function<void(const std::string &, const std::string &, const std::chrono::seconds &, const int &)>> subscriptions_;

    std::function<void()> connect_callback_;
    std::function<void()> disconnect_callback_;


    QString broker_addr_;
    int_fast16_t broker_port_;
    QString client_id_;
};

#endif // MQTT_H
