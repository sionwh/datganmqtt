/** @file
 * @brief Implementation of the BrokerConfig class which is the UI for setting and dispalying connection
 * profile attributes
 *
 * Copyright 2020 Siôn Hughes
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, 
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or 
 * substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 **/
#include <QFileDialog>
#include <iostream>

#include "brokerconfig.h"
#include "ui_brokerconfig.h"
#include "brokerconfigfile.h"

/**
 * @brief Constructor, sets up the UI
 * @param parent Pointer to the parent widget
 **/
BrokerConfig::BrokerConfig(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::BrokerConfig)
{
    ui->setupUi(this);
}

/**
 * @brief Destructor
 **/
BrokerConfig::~BrokerConfig()
{
    delete ui;
}

/**
 * @brief Sets UI fields (broker address, port, etc) from the connection profile in the config file at the given path.
 * @param path Path to the config file to read broker connection configuration from
 **/
void BrokerConfig::SetUIFromConfig(QString path)
{
    BrokerConfigFile config_file(path.toStdString());
    ui->te_ConnectionName->setText(QString::fromStdString(config_file.GetProfileName()));
    ui->te_BrokerAddress->setText(QString::fromStdString(config_file.GetHost()));
    ui->te_BrokerPort->setText(QString::number(config_file.GetPort()));
    ui->te_ClientID->setText(QString::fromStdString(config_file.GetClientID()));

    ui->check_TLS->setCheckState( config_file.GetTlsEnable() ? Qt::CheckState::Checked : Qt::CheckState::Unchecked);
    ui->te_CA->setText(QString::fromStdString(config_file.GetCAPath()));
    ui->te_Cert->setText(QString::fromStdString(config_file.GetCertPath()));
    ui->te_Key->setText(QString::fromStdString(config_file.GetKeyPath()));

    ui->te_Username->setText(QString::fromStdString(config_file.GetUsername()));
    ui->te_Password->setText(QString::fromStdString(config_file.GetPassword()));
}

/**
 * @brief Sets the values from UI fields to the connection profile and writes out to disk, overwriting previous file
 **/
void BrokerConfig::on_buttonBox_accepted()
{
    // Write out to config file
    BrokerConfigFile config_file;
    config_file.SetHost(ui->te_BrokerAddress->toPlainText().toStdString());
    config_file.SetPort(ui->te_BrokerPort->toPlainText().toInt());
    config_file.SetProfileName(ui->te_ConnectionName->toPlainText().toStdString());
    config_file.SetClientID(ui->te_ClientID->toPlainText().toStdString());

    config_file.SetTlsEnable(ui->check_TLS->checkState() == Qt::CheckState::Checked ? true : false);
    config_file.SetCAPath(ui->te_CA->toPlainText().toStdString());
    config_file.SetCertPath(ui->te_Cert->toPlainText().toStdString());
    config_file.SetKeyPath(ui->te_Key->toPlainText().toStdString());

    config_file.SetUsername(ui->te_Username->toPlainText().toStdString());
    config_file.SetPassword(ui->te_Password->toPlainText().toStdString());

    config_file.Write();
}

/**
 * @brief Enables UI elements related to TLS parameters
 * @param state TLS Connection state (true = enable TLS, false = disable TLS)
 **/
void BrokerConfig::on_check_TLS_stateChanged(int state)
{
    if (state == Qt::Checked)
    {
        ui->te_CA->setEnabled(true);
        ui->te_Cert->setEnabled(true);
        ui->te_Key->setEnabled(true);
        ui->pb_CA->setEnabled(true);
        ui->pb_Cert->setEnabled(true);
        ui->pb_Key->setEnabled(true);
    }
    else if (state == Qt::Unchecked)
    {
        ui->te_CA->setEnabled(false);
        ui->te_Cert->setEnabled(false);
        ui->te_Key->setEnabled(false);
        ui->pb_CA->setEnabled(false);
        ui->pb_Cert->setEnabled(false);
        ui->pb_Key->setEnabled(false);
    }
}

/**
 * @brief Opens file dialog to select a .pem or .crt file which contains the broker to connect to's CA. UI element set once selected
 **/
void BrokerConfig::on_pb_CA_clicked()
{
    std::vector<std::pair<QString,QString>> types;
    types.push_back(std::make_pair("PEM CA File","pem"));
    types.push_back(std::make_pair("CA File","crt"));

    QString path = LoadFromFile(types);
    if(!path.isEmpty())
    {
        ui->te_CA->setText(path);
    }
}

/**
 * @brief Opens file dialog to select a .crt file which contains the certificate to use for connecting to broker. UI element set once selected
 **/
void BrokerConfig::on_pb_Cert_clicked()
{
    QString path = LoadFromFile("Cert File", "crt");
    if (!path.isEmpty())
    {
        ui->te_Cert->setText(path);
    }
}

/**
 * @brief Opens file dialog to select a .key file which contains the key to use for connecting to broker. UI element set once selected
 **/
void BrokerConfig::on_pb_Key_clicked()
{
    QString path = LoadFromFile("Key File", "key");
    if (!path.isEmpty())
    {
        ui->te_Key->setText(path);
    }
}

/**
 * @brief Opens a file dialog with the given filters and returns path to the selected file
 * @param type Description of the file type to use in file dialog filter
 * @param extension The extension to use in file dialog filter
 * @param path Path to open file dialog at, defaults to home directory
 * @return path to the file selected, empty if no file selected
 **/
QString BrokerConfig::LoadFromFile(QString type, QString extension, QString path)
{
    return QFileDialog::getOpenFileName(this, ("Open File"),
                                                      path,
                                                      (type + " (*." +extension +")"));
}

/**
 * @brief Opens a file dialog with the given filters and returns path to the selected file
 * @param types Vector of pairs of file type and extension to use in the file dialog filter
 * @param path Path to open file dialog at, defaults to home directory
 * @return path to the file selected, empty if no file selected
 **/
QString BrokerConfig::LoadFromFile(std::vector<std::pair<QString,QString>> types, QString path)
{
    QString file_types;

    for(const auto & t:types)
    {
        file_types += (t.first + " (*." +t.second +");;");
    }

    return QFileDialog::getOpenFileName(this, ("Open File"),
                                                      path,
                                                      file_types);
}
