/** @file
 * @brief Implementation of the BrokerConfigFile class which is responsible for reading and writing connection
 * profiles to and from config files on the system
 *
 * Copyright 2020 Siôn Hughes
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, 
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or 
 * substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 **/

#include <sstream>
#include <fstream>
#include <iostream>
#include <filesystem>
#include <locale>
#include <algorithm>

#include "brokerconfigfile.h"

const std::string BrokerConfigFile::CONFIG_EXT_{".json"};
const std::string BrokerConfigFile::CONFIG_DIR_{std::string(std::getenv("HOME"))+"/.mqtttool"};


/**
 * @brief Constructor taking a path to the filename of a broker connection profile. If the file at path exists, the broker connection profile is read in.
 * @param path Path to the config file to read broker connection configuration from
 **/
BrokerConfigFile::BrokerConfigFile(std::string path):
    path_(path)
{
    if (std::filesystem::exists(path))
    {
        Read();
    }
}

/**
 * @brief Opens and parses JSON for broker connection profile. If the profile can't be opened,
 *      is invalid JSON or doesn't have all the necessary configuration no internal members are modified
 * @return true If broker connection configuration file is read and parsed correctly. 
 **/
bool BrokerConfigFile::Read()
{
    bool file_good = false;
    std::fstream file;
    file.open(path_,std::fstream::in);
    std::stringstream contents;

    // Read the file in
    if (file.is_open())
    {
        contents << file.rdbuf();

        // Create the JSON contents
        try
        {
            contents >> json_contents_;    
        }
        catch(const std::exception& e)
        {
            std::cerr << "Error parsing JSON" << std::endl;
            return false;
        }      

        // Extract the Key/Value pairs we need
        if (!json_contents_.empty())
        {
            bool parse_error = false;

            if (json_contents_["ProfileName"].is_string() &&
                json_contents_["Host"].is_string() &&
                json_contents_["Port"].is_number_integer() && 
                json_contents_["ClientID"].is_string() )
            {
                // Grab the Mandatory items
                profile_name_ = json_contents_["ProfileName"];
                host_ = json_contents_["Host"];
                port_ = json_contents_["Port"];
                clientid_ = json_contents_["ClientID"];

                if (json_contents_["Credentials"]["Username"].is_string())
                {
                    username_ = json_contents_["Credentials"]["Username"];
                }

                if (json_contents_["Credentials"]["Password"].is_string())
                {
                    password_ = json_contents_["Credentials"]["Password"];
                }

                if (json_contents_["TLS"]["Enabled"].is_boolean())
                {
                    tls_enable_ = json_contents_["TLS"]["Enabled"];
                }
                
                if (json_contents_["TLS"]["CA"].is_string())
                {
                    ca_path_ = json_contents_["TLS"]["CA"];
                }

                if (json_contents_["TLS"]["Cert"].is_string())
                {
                    cert_path_ = json_contents_["TLS"]["Cert"];
                }

                if (json_contents_["TLS"]["Key"].is_string())
                {
                    key_path_ = json_contents_["TLS"]["Key"];
                }
            }
            else
            {
                parse_error=true;
            }

            if (!parse_error)
            {
                file_good = true;
            }
        }
    }

    return file_good;
}

/**
 * @brief Writes out the current broker connection profile out to disk. If the config directory doesn't exist, it is created.
 *      If the profile already exists, it is overwritten rather than updated element by element (anything added in file which
 *      isn't written back out will have been added by hand)
 **/
void BrokerConfigFile::Write()
{
    // Create the config dir if needed
    CreateConfigDir();

    // Does the file exist?
    if (std::filesystem::exists(path_))
    {
        std::filesystem::remove(path_);
    }

    // Update the JSON contents member
    json_contents_["ProfileName"] = profile_name_;
    json_contents_["Host"] = host_;
    json_contents_["Port"] = port_;
    json_contents_["ClientID"] = clientid_;
    json_contents_["Credentials"]["Username"] = username_;
    json_contents_["Credentials"]["Password"] = password_;
    json_contents_["TLS"]["Enabled"] = tls_enable_;
    json_contents_["TLS"]["CA"] = ca_path_;
    json_contents_["TLS"]["Cert"] = cert_path_;
    json_contents_["TLS"]["Key"] = key_path_;

    // Write out to file
    std::fstream file;
    file.open(path_, std::fstream::out);
    file << json_contents_;
    file.close();
}

/**
 * @brief Creates a configuration directory if one doesn't already exists at the expected path
 **/
void BrokerConfigFile::CreateConfigDir()
{
    if (!std::filesystem::exists(CONFIG_DIR_))
    {
        std::filesystem::create_directory(CONFIG_DIR_);
    }
}

/**
 * @brief Gets the path to the broker connection profile represented by this object
 * @return Path of the broker connection profile file
 **/
std::string BrokerConfigFile::GetPath()
{
    return path_;
}

/**
 * @brief Gets the name of the broker connection profile represented by this object
 * @return Name of the broker connection profile
 **/
std::string BrokerConfigFile::GetProfileName()
{
    return profile_name_;
}

/**
 * @brief Gets the broker host name of the connection profile represented by this object
 * @return Broker host name
 **/
std::string BrokerConfigFile::GetHost()
{
    return host_;
}

/**
 * @brief Gets the broker host port of the connection profile represented by this object
 * @return Broker host port
 **/
int_fast16_t BrokerConfigFile::GetPort()
{
    return port_;
}

/**
 * @brief Gets the specified client ID of the connection profile represented by this object
 * @return Client ID
 **/
std::string BrokerConfigFile::GetClientID()
{
    return clientid_;
}

/**
 * @brief Gets the username to use on connection to the broker of the connection profile represented by this object
 * @return Username of the broker connection profile
 **/
std::string BrokerConfigFile::GetUsername()
{
    return username_;
}

/**
 * @brief Gets the password to use on connection to the broker of the connection profile represented by this object
 * @return Password of the broker connection profile
 **/
std::string BrokerConfigFile::GetPassword()
{
    return password_;
}

/**
 * @brief Gets the state of TLS enable option to use on connection to the broker of the connection profile represented by this object
 * @return TLS enable option state (true = enabled, false = disabled)
 **/
bool BrokerConfigFile::GetTlsEnable()
{
    return tls_enable_;
}

/**
 * @brief Gets the path to the CA file to use in broker connection profile represented by this object
 * @return Path of the CA file to use in broker connection profile
 **/
std::string BrokerConfigFile::GetCAPath()
{
    return ca_path_;
}

/**
 * @brief Gets the path to the Certificate file to use in broker connection profile represented by this object
 * @return Path of the Certiifcate file to use in broker connection profile
 **/
std::string BrokerConfigFile::GetCertPath()
{
    return cert_path_;
}

/**
 * @brief Gets the path to the Key file to use in broker connection profile represented by this object
 * @return Path of the Key file to use in broker connection profile
 **/
std::string BrokerConfigFile::GetKeyPath()
{
    return key_path_;
}

/**
 * @brief Gets a JSON representation of the broker connection profile represented by this object
 * @return JSON representation of the broker connection profile
 **/
json BrokerConfigFile::GetJson()
{
    return json_contents_;
}

/**
 * @brief Sets the path to the broker connection profile represented by this object
 * @param path Path of the broker connection profile file
 **/
void BrokerConfigFile::SetPath(std::string path)
{
    path_ = path;
}

/**
 * @brief Sets the name of the broker connection profile represented by this object. 
 *      Non alpha numeric characters are removed to create a filename which can be written to disk
 * @param Name of the broker connection profile
 * @returns The cleaned up profile name
 **/
std::string BrokerConfigFile::SetProfileName(std::string name)
{
    profile_name_ = name;

    // Set the config path
    std::string cleaned_name = name;
    cleaned_name.erase(std::remove_if(cleaned_name.begin(), cleaned_name.end(),
						[](char &c) {
							return !std::isalpha<char>(c, std::locale::classic());
						}),
						cleaned_name.end());
    path_ = CONFIG_DIR_ + "/" + cleaned_name + CONFIG_EXT_;
    return cleaned_name;
}

/**
 * @brief Sets the broker host name of the connection profile represented by this object
 * @param host Broker host name/IP
 **/
void BrokerConfigFile::SetHost(std::string host)
{
    host_ = host;
}

/**
 * @brief Sets the broker host port of the connection profile represented by this object
 * @param port Broker host port to use for this connection profile
 **/
void BrokerConfigFile::SetPort(int_fast16_t port)
{
    port_ = port;
}

/**
 * @brief Sets the specified client ID of the connection profile represented by this object
 * @param clientid Client ID to use for this connection profile
 **/
void BrokerConfigFile::SetClientID(std::string clientid)
{
    clientid_ = clientid;
}


/**
 * @brief sets the username to use on connection to the broker of the connection profile represented by this object
 * @param username Username of the broker connection profile
 **/
void BrokerConfigFile::SetUsername(std::string username)
{
    username_ = username;
}

/**
 * @brief Sets the password to use on connection to the broker of the connection profile represented by this object
 * @param password Password of the broker connection profile
 **/
void BrokerConfigFile::SetPassword(std::string password)
{
    password_ = password;
}

/**
 * @brief Sets the state of TLS enable option to use on connection to the broker of the connection profile represented by this object
 * @param tls_enable TLS enable option state (true = enabled, false = disabled)
 **/
void BrokerConfigFile::SetTlsEnable(bool tls_enable)
{
    tls_enable_ = tls_enable;
}

/**
 * @brief Sets the path to the CA file to use in broker connection profile represented by this object
 * @param ca_path Path of the CA file to use in broker connection profile
 **/

void BrokerConfigFile::SetCAPath(std::string ca_path)
{
    ca_path_ = ca_path;
}

/**
 * @brief Sets the path to the Certificate file to use in broker connection profile represented by this object
 * @param cert_path Path of the Certificate file to use in broker connection profile
 **/

void BrokerConfigFile::SetCertPath(std::string cert_path)
{
    cert_path_ = cert_path;
}

/**
 * @brief Sets the path to the Key file to use in broker connection profile represented by this object
 * @param key_path Path of the Key file to use in broker connection profile
 **/
void BrokerConfigFile::SetKeyPath(std::string key_path)
{
    key_path_ = key_path;
}
