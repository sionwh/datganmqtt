/** @file
 * @brief Implementation of the MainWindow class which is the main UI for the Datgan Application
 *
 * Copyright 2020 Siôn Hughes
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 **/
#include <iostream>
#include <filesystem>
#include <chrono>
#include <QFile>
#include <QTextStream>
#include <QMessageBox>
#include <chrono>

#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include "brokerconfig.h"
#include "brokerconfigfile.h"
#include "receivedmessagedelegate.h"
#include "plugins/text/textplugin.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // Populate the Connection Combo
    PopulateConnections();

    qRegisterMetaType<std::chrono::seconds>("std::chrono::seconds");
    connect(this, SIGNAL( MessageReceivedSignal(QString, QString, std::chrono::seconds, int) ),
            this, SLOT( MessageReceivedSlot(QString, QString, std::chrono::seconds, int) ) );


    connect(this, SIGNAL( ConnectionStateChangedSignal(bool) ),
            this, SLOT( ConnectionStateChangedSlot(bool) ) );

    // Connect up event of changed highlighted item in the message list to show in the payload/topic view.
    //  Allows navigation of list via keyboard
    connect(ui->listView->selectionModel(), SIGNAL(currentChanged(const QModelIndex &, const QModelIndex &)),
                this, SLOT(on_listView_clicked(const QModelIndex &)));

    // Load and populate available plugins
    LoadPlugins();
    PopulatePlugins();

    // Set the Pause Scroll button to be checkable, turn it red once checked
    ui->pb_PauseScroll->setCheckable(true);
    ui->pb_PauseScroll->setStyleSheet("\
            QPushButton:checked{background-color:red;}");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::showEvent(QShowEvent *event)
{
    // Call the QMainWindow implementation first
    QMainWindow::showEvent( event );
}


void MainWindow::PopulateConnections()
{
    std::cerr << "Populating connections" << std::endl;
    // Remove all items
    ui->cb_Connection->clear();
    connection_configs_.clear();

    try
    {
        // Then read in saved connections
        for (const auto & file : std::filesystem::directory_iterator(BrokerConfigFile::CONFIG_DIR_))
        {
            if(file.path().extension() == BrokerConfigFile::CONFIG_EXT_)
            {
                // Is it Valid?
                BrokerConfigFile config(file.path().string());
                if (config.Read())
                {
                    connection_configs_.insert(std::pair<std::string, std::string>(config.GetProfileName(),file.path().string()));
                    ui->cb_Connection->addItem(QString::fromStdString(config.GetProfileName()));
                }
            }
        }
    }
    catch(const std::exception& e)
    {
        std::cerr << "Config dir doesn't exist" << std::endl;
    }

    // Add in a "New Connection"
    ui->cb_Connection->addItem(newconnect_);

    // Set the current index to be blank
    ui->cb_Connection->setCurrentIndex(-1);
}


void MainWindow::on_cb_Connection_activated(int index)
{
    // If user wants to add a new connection
    if ((ui->cb_Connection->count() -1) == index)
    {
        BrokerConfig *broker_config = new BrokerConfig();
        broker_config->exec();

        // A new config could have been
        PopulateConnections();
    }
    else
    {
        connection_config_ = new BrokerConfigFile(connection_configs_[ui->cb_Connection->currentText().toStdString()]);
        if (connection_config_->Read())
        {
            // Enable the settings button
            ui->pb_ConnectionSettings->setEnabled(true);
            ui->pb_Connect->setEnabled(true);
        }
    }
}

void MainWindow::on_pb_Connect_clicked()
{

    //Disable the button, set a connecting message
    ui->pb_Connect->setEnabled(false);
    ui->lbl_ConnectionError->setText("Connecting");
    ui->lbl_ConnectionError->setStyleSheet("color: orange;");

    // Enable the Cancel button
    ui->pb_Cancel->setEnabled(true);

    qApp->processEvents();

    try
    {
        // Try and connect to the broker
        std::cerr << "Will connect to " << connection_config_->GetHost() << std::endl;
        mqtt.Initialise(QString::fromStdString(connection_config_->GetHost()), connection_config_->GetPort(), QString::fromStdString(connection_config_->GetClientID()));

        if (connection_config_->GetTlsEnable())
        {
            mqtt.Connect(this->ConnectCallback, this->DisconnectCallback, connection_config_->GetUsername(), connection_config_->GetPassword(), connection_config_->GetCAPath(), connection_config_->GetKeyPath(), connection_config_->GetCertPath());
        }
        else
        {
            mqtt.Connect(this->ConnectCallback, this->DisconnectCallback, connection_config_->GetUsername(), connection_config_->GetPassword());
        }

    }
    catch (std::runtime_error re)
    {
        ui->lbl_ConnectionError->setText(re.what());
        ui->lbl_ConnectionError->setStyleSheet("color: red;");

        // Re-enable the connect button to start again
        ui->pb_Connect->setEnabled(true);

        // Destory the mqtt instance
        mqtt.Destroy();
        return;
    }
}

void MainWindow::on_pb_Subscribe_clicked()
{
    try
    {
        mqtt.Subscribe(ui->te_SubTopic->toPlainText(), this->MessageReceivedCallback, ui->spin_SubQos->value());
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << std::endl;
        return;
    }

    // Must be OK
    ui->lv_subscriptions->AddSubscription(ui->te_SubTopic->toPlainText().toStdString());

}

void MainWindow::on_pb_Disconnect_clicked()
{
    mqtt.Disconnect();

    DisconnectCleanup();
}

void MainWindow::DisconnectCleanup()
{
    mqtt.Destroy();

    ui->pb_Disconnect->setEnabled(false);
    ui->pb_Connect->setEnabled(true);
    ui->pb_Subscribe->setEnabled(false);
    ui->te_SubTopic->setEnabled(false);
    ui->pb_Cancel->setEnabled(false);

    ui->lbl_ConnectionError->setText("Disconnected");
    ui->lbl_ConnectionError->setStyleSheet("color: red;");
}


void MainWindow::MessageReceivedSlot(QString topic, QString payload, std::chrono::seconds timestamp, int qos)
{
    // Add the message to the listview
    ui->listView->AddMessage(topic, payload,timestamp,qos);
    messages_received_++;
    ui->lbl_MessageCount->setText(QString::number(messages_received_));

    for(int i = 0; i < ui->lv_subscriptions->model()->rowCount(); ++i)
    {
        QModelIndex index = ui->lv_subscriptions->model()->index(i, 0);
        QString subscription = ui->lv_subscriptions->model()->data(index, Qt::UserRole).toString();

        // Match the received topic with the subscriptions
        if(mqtt.DoesTopicMatchSub(subscription, topic))
        {
            int32_t count = ui->lv_subscriptions->model()->data(index, Qt::UserRole+1).toInt();
            count++;
            ui->lv_subscriptions->model()->setData(index, count, Qt::UserRole+1);
        }
    }

    // Enable the clear message button, as there now are messages to clear
    ui->pb_ClearMessages->setEnabled(true);
}

void MainWindow::MessageReceivedCallback(std::string topic, std::string payload, std::chrono::seconds timestamp, int qos)
{
    QWidgetList wl = QApplication::topLevelWidgets();
    foreach (QWidget *widget, wl)
    {
        if (MainWindow *mw = qobject_cast<MainWindow *>(widget))
        {
            emit(mw->MessageReceivedSignal(QString::fromStdString(topic), QString::fromStdString(payload), timestamp, qos));
        }
    }
}

void MainWindow::on_pb_ConnectionSettings_clicked()
{
    QString path = QString::fromStdString(connection_configs_[ui->cb_Connection->currentText().toStdString()]);
    BrokerConfig *broker_config = new BrokerConfig();
    broker_config->SetUIFromConfig(path);
    broker_config->exec();

    //Reload all configs in
    connection_config_ = new BrokerConfigFile(connection_configs_[ui->cb_Connection->currentText().toStdString()]);
    connection_config_->Read();
}

void MainWindow::on_listView_clicked(const QModelIndex &index)
{
    QString topic = index.data(Qt::UserRole).toString();
    QString payload = index.data(Qt::UserRole+1).toString();


    ui->te_SelectedTopic->setText(topic);

    // Use the selected plugin to format data
    FormatAndShowMessagePayload(payload);
}

void MainWindow::on_lv_subscriptions_clicked(const QModelIndex &index)
{
    ui->pb_RemoveSubscription->setEnabled(true);
}

void MainWindow::on_pb_RemoveSubscription_clicked()
{
    // Get the currently selected subscription
    QModelIndex selected_index = ui->lv_subscriptions->currentIndex();

    // Now unsubscribe then delete from the list
    QString topic = selected_index.data(Qt::UserRole).toString();
    mqtt.Unsubscribe(topic);
    ui->lv_subscriptions->model()->removeRow(selected_index.row());
}

void MainWindow::on_btn_night_clicked()
{
    if (!night_mode_)
    {
        // Load theme
        QFile f(":qdarkstyle/style.qss");

        if (!f.exists())
        {
            std::cerr << "Unable to set stylesheet, file not found" << std::endl;
        }
        else
        {
            f.open(QFile::ReadOnly | QFile::Text);
            QTextStream ts(&f);
            qApp->setStyleSheet(ts.readAll());
            night_mode_ = true;
        }
    }
    else
    {
        qApp->setStyleSheet("");
        night_mode_ = false;
    }
}


void MainWindow::LoadPlugins()
{
    // Always load in TextPlugin
    TextPlugin* text_plugin = new TextPlugin();
    message_format_plugins_.insert(std::pair<QString, IPayloadParserplugin*>(text_plugin->GetName(),text_plugin));

    // Does plugin dir exist?
    if (std::filesystem::exists(plugins_dir_))
    {
        for(const auto& pluginfile: std::filesystem::directory_iterator(plugins_dir_))
        {
            if (pluginfile.path().extension() == plugins_ext_)
            {
                QPluginLoader loader(QString::fromStdString(pluginfile.path()));
                if (auto instance = loader.instance())
                {
                    if (auto plugin = qobject_cast< IPayloadParserplugin* >(instance))
                    {
                        std::cerr << "Adding " << plugin->GetName().toStdString() << std::endl;
                        message_format_plugins_.insert(std::pair<QString, IPayloadParserplugin*>(plugin->GetName(),plugin));
                    }
                    else
                    {
                        std::cout << "qobject_cast<> returned nullptr" << std::endl;
                    }
                }
                else
                {
                std::cout << loader.errorString().toStdString() << std::endl;
                }
            }
        }
    }
    else
    {
        std::cerr << "No plugins found" << std::endl;
    }
}

void MainWindow::PopulatePlugins()
{
    std::cerr << "Populating plugins" << std::endl;
    ui->cb_Plugins->clear();
    for (const auto& plugin: message_format_plugins_)
    {
        ui->cb_Plugins->addItem(plugin.first);
    }

    // Always set the default on startup to selected to "Text"
    ui->cb_Plugins->setCurrentText("Text");


    // Enable it
    ui->cb_Plugins->setEnabled(true);
}

void MainWindow::on_cb_Plugins_currentIndexChanged(const QString &arg1)
{
    // The selected plugin has changed, we need to update the parsing of the text
    currentSelectedPlugin = ui->cb_Plugins->currentText();

    QModelIndex index = ui->listView->currentIndex();
    FormatAndShowMessagePayload(index.data(Qt::UserRole+1).toString());
}

void MainWindow::FormatAndShowMessagePayload(QString payload)
{
    QString formatted{""};
    bool parse_result = message_format_plugins_[currentSelectedPlugin]->ParseAndFormat(payload, formatted);

    ui->te_SelectedPayload->setText(formatted);

    if (parse_result)
    {
        QPalette palette = ui->te_SelectedPayload->palette();
        palette.setColor(QPalette::Base, message_parse_success_colour_);
        ui->te_SelectedPayload->setPalette(palette);
    }
    else
    {
        QPalette palette = ui->te_SelectedPayload->palette();
        palette.setColor(QPalette::Base, message_parse_fail_colour_);
        ui->te_SelectedPayload->setPalette(palette);
    }
}


void MainWindow::ConnectCallback()
{
    std::cout << "Connected" << std::endl;

    QWidgetList wl = QApplication::topLevelWidgets();
    foreach (QWidget *widget, wl)
    {
        if (MainWindow *mw = qobject_cast<MainWindow *>(widget))
        {
            emit(mw->ConnectionStateChangedSignal(true));
        }
    }
}

void MainWindow::DisconnectCallback()
{
    QWidgetList wl = QApplication::topLevelWidgets();
    foreach (QWidget *widget, wl)
    {
        if (MainWindow *mw = qobject_cast<MainWindow *>(widget))
        {
            emit(mw->ConnectionStateChangedSignal(false));
        }
    }
}

void MainWindow::ConnectionStateChangedSlot(bool state)
{
    if (state)
    {
        ui->pb_Connect->setEnabled(false);
        ui->pb_Disconnect->setEnabled(true);
        ui->pb_Cancel->setEnabled(false);
        ui->pb_Subscribe->setEnabled(true);
        ui->te_SubTopic->setEnabled(true);

        // Connection was successful
        ui->lbl_ConnectionError->setText("Successfully connected");
        ui->lbl_ConnectionError->setStyleSheet("color: green;");

        // Remove all old subscriptions and messages
        ClearReceivedMessages();
        ClearSubscriptions();
        ClearPublishedMessages();
    }
    else
    {
        ui->pb_Connect->setEnabled(true);
        DisconnectCleanup();
    }

}

void MainWindow::on_pb_ClearMessages_clicked()
{
    // Ask if the user meant this...
    QMessageBox msgBox;
    msgBox.setWindowTitle("Clear messages?");
    msgBox.setText("Do you want to clear all messages?");
    msgBox.setStandardButtons(QMessageBox::Yes);
    msgBox.addButton(QMessageBox::No);
    msgBox.setDefaultButton(QMessageBox::No);

    // If they did...
    if(msgBox.exec() == QMessageBox::Yes)
    {
        // Clear all messages
        ClearReceivedMessages();
    }

    // Disable the clear message button, as nothing to clear!!
    ui->pb_ClearMessages->setEnabled(false);

    // Clear the message from the focused view
    ui->te_SelectedPayload->setText("");
    ui->te_SelectedTopic->setText("");
}

void MainWindow::ClearReceivedMessages()
{
    ui->listView->model()->removeRows(0,ui->listView->model()->rowCount());

    // Reset message counters
    messages_received_ = 0;
    ui->lbl_MessageCount->setText(QString::number(messages_received_));

    for(int i = 0; i < ui->lv_subscriptions->model()->rowCount(); ++i)
    {
        QModelIndex index = ui->lv_subscriptions->model()->index(i, 0);
        ui->lv_subscriptions->model()->setData(index, 0, Qt::UserRole+1);

    }
}

void MainWindow::ClearPublishedMessages()
{
    ui->lv_SentMessages->model()->removeRows(0,ui->lv_SentMessages->model()->rowCount());
}

void MainWindow::ClearSubscriptions()
{
    ui->lv_subscriptions->model()->removeRows(0,ui->lv_subscriptions->model()->rowCount());

    ui->te_SubTopic->setText("");
}

void MainWindow::on_pb_Publish_clicked()
{
    QString topic = ui->te_Topic->toPlainText();
    QString payload = ui->te_Payload->toPlainText();
    int qos = ui->spin_Qos->value();
    bool retained = ui->check_Retained->isChecked();
    std::chrono::seconds timestamp = std::chrono::seconds(std::time(nullptr));

    mqtt.Publish(topic, payload, qos, retained);

    ui->lv_SentMessages->AddMessage(topic, payload, timestamp, qos);
}

void MainWindow::on_pb_Cancel_clicked()
{
    // Force stop and destroy the instance
    mqtt.Destroy();

    DisconnectCleanup();
}

void MainWindow::on_lv_SentMessages_clicked(const QModelIndex &index)
{
    QString payload = index.data(Qt::UserRole+1).toString();

    ui->te_SelectedPublishedPayload->setText(payload);
}


void MainWindow::on_pb_PauseScroll_clicked()
{
    pause_message_scroll_ = !pause_message_scroll_;
    ui->listView->SetScrollOnNewMessage(pause_message_scroll_);
}
