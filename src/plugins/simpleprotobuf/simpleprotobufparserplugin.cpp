#include <QDebug>
#include <QByteArray>

#include <google/protobuf/util/json_util.h>
#include "simpleprotobufparserplugin.h"

QString SimpleProtobufParserPlugin::GetName() const
{
    return "Simple Protobuf Parser";
}

bool SimpleProtobufParserPlugin::ParseAndFormat(const QString& input, QString & output) const
{
    bool parse_result{true};

    //base64 decode
    auto decoded = QByteArray::fromBase64(input.toUtf8());

    Simple simple;

    // Attempt to parse the protocol buffer data
    if(simple.ParseFromString(std::string(decoded.begin(), decoded.end())))
    {
        // Set options
        google::protobuf::util::JsonPrintOptions options;
        options.add_whitespace = true; // We want whitespace for pretty printing

        // Do the message to JSON conversion
        std::string parsed_output;
        google::protobuf::util::MessageToJsonString(simple, &parsed_output, options);

        // Set the output to be the JSON output
        output = QString::fromStdString(parsed_output);

        // All parsed ok 
        parse_result = true;
    }
    else
    {
        // Error in parsing so just return the string we had before
        output = input;

        // All didn't parse OK
        parse_result = false;
    }

    return parse_result;
}

