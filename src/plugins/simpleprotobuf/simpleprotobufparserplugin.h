#include <QObject>
#include <QtPlugin>

#include "../ipayloadparserplugin.h"
#include "simple.pb.h"

class SimpleProtobufParserPlugin : public QObject, public IPayloadParserplugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID IPayloadParserPlugin_iid)
    Q_INTERFACES(IPayloadParserplugin)

public:
    ~SimpleProtobufParserPlugin() override {}
    bool ParseAndFormat(const QString& input, QString & output) const override;
    QString GetName() const override;
};