#ifndef TEXTPARSERPLUGIN_H
#define TEXTPARSERPLUGIN_H

#include <QObject>
#include <QtPlugin>
#include "../ipayloadparserplugin.h"

class TextPlugin : public QObject, public IPayloadParserplugin
{
    Q_OBJECT
    Q_INTERFACES(IPayloadParserplugin)

public:
    ~TextPlugin() override {}
    bool ParseAndFormat(const QString& input, QString & output) const override;
    QString GetName() const override;
};

#endif