#ifndef IPAYLOADPARSERPLUGIN_H
#define IPAYLOADPARSERPLUGIN_H
#include <QtCore/qglobal.h>

class IPayloadParserplugin
{
    
public:
    virtual bool ParseAndFormat(const QString& input, QString & output) const = 0;
    virtual QString GetName() const = 0;
};

#define IPayloadParserPlugin_iid "com.sionhughes.datgan.IPayloadParserplugin"
Q_DECLARE_INTERFACE( IPayloadParserplugin, IPayloadParserPlugin_iid)

#endif //IPAYLOADPARSERPLUGIN_H