#include <QDebug>

#include <nlohmann/json.hpp>

#include "jsonparserplugin.h"


using json = nlohmann::json;

QString JsonParserPlugin::GetName() const
{
    return "JSON Parser";
}

bool JsonParserPlugin::ParseAndFormat(const QString& input, QString & output) const
{
    bool parse_result{true};

    // Attempt to parse the input into JSON
    try
    {
        json payload = json::parse(input.toStdString());

        // Output must be OK, otherwise we'd have thrown exctpion
        parse_result = true;

        // Foramt and store the json to the output
        std::string formatted = payload.dump(2, /*indentation level of 2*/
                                ' ', /*use spaces*/
                                true /*ensure ASCII*/);
        output = QString::fromStdString(formatted);
    }
    catch (json::parse_error& e)
    {
        parse_result = false;
        output = input;
    }


    return parse_result;
}

