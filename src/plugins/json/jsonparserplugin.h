#ifndef JSONPARSERPLUGIN_H
#define JSONPARSERPLUGIN_H

#include <QObject>
#include <QtPlugin>
#include "../ipayloadparserplugin.h"

class JsonParserPlugin : public QObject, public IPayloadParserplugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "com.sionhughes.datgan.JsonParserPlugin")
    Q_INTERFACES(IPayloadParserplugin)

public:
    ~JsonParserPlugin() override {}
    bool ParseAndFormat(const QString& input, QString & output) const override;
    QString GetName() const override;
};

#endif