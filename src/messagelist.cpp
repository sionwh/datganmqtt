/** @file
 * @brief Implementation of the MessageList class which is responsible for showing a list of MQTT messages
 *
 * Copyright 2020 Siôn Hughes
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, 
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or 
 * substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 **/
#include <QStandardItemModel>
#include <QString>
#include <iostream>

#include "messagelist.h"
#include "receivedmessagedelegate.h"

MessageList::MessageList(QWidget *parent):
	QListView(parent)
{
    auto *delegate = new ReceivedMessageDelegate(this);

    setModel(new QStandardItemModel(this));
	setItemDelegate(delegate);
}

void MessageList::ClearMessages()
{
    static_cast<QStandardItemModel *>(model())->clear();
}

void MessageList::AddMessage(QString topic, QString message, std::chrono::seconds timestamp, int qos)
{
    auto *item = new QStandardItem();

	item->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
    item->setData(topic,Qt::UserRole);
	item->setData(message,Qt::UserRole+1);
    item->setData(QVariant::fromValue(timestamp.count()),Qt::UserRole+2);
    item->setData(QVariant::fromValue(qos),Qt::UserRole+3);

	static_cast<QStandardItemModel *>(model())->appendRow(item);

    if (auto_scroll_enabled_)
    {
        scrollToBottom();
    }
}

/**
 * @brief Sets whether the message list should auto scroll on new message added
 * 
 * @param enable - Enable auto scroll if true, disable otherwise
 **/
void MessageList::SetScrollOnNewMessage(bool enable)
{
    auto_scroll_enabled_ = enable;
}