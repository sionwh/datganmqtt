/** @file
 * @brief Implementation of the Mqtt class which is responsible for MQTT communications between the application and a broker
 * Provides methods for connecting, subscribing and publishing
 *
 * Copyright 2020 Siôn Hughes
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 **/
#include<QException>

#include <mosquitto.h>
#include <stdexcept>
#include <iostream>

#include "mqtt.h"


/**
 * @brief Initialise the MQTT connection and set the internall callbacks for message received,
 *      connection and disconnection events. Broker is told to clean all messages and subscriptions
 *      on disconnect
 *
 * @param broker_addr The address of the broker to connect to
 * @param broker_port The port of the broker to connect to
 * @param client_it The ID of this client to inform thr broker
 * @throws runtime_error If error in creating mosquitto instance
 **/
void Mqtt::Initialise(const QString& broker_addr, const int_fast16_t broker_port, const QString& client_id)
{
    // Store the args
    broker_addr_ = broker_addr;
    broker_port_ = broker_port;
    client_id_ = client_id;

    // Create connection
    if (!mosquitto_conn_)
    {
        mosquitto_conn_ = mosquitto_new(client_id_.toStdString().c_str(), true, this);
    }

    // Check connection
    if(!mosquitto_conn_)
    {
       throw std::runtime_error("Error in MQTT initialisation");
    }

    // Register callbacks
    mosquitto_message_callback_set(mosquitto_conn_, &Mqtt::MessageReceivedCallback);
    mosquitto_connect_callback_set(mosquitto_conn_, &Mqtt::ConnectCallback);
    mosquitto_disconnect_callback_set(mosquitto_conn_, &Mqtt::DisconnectCallback);
}


/**
 * @brief Start the mosquitto loop and connect to the broker previously configured. Always uses TLSv1.2 for now. Uses the async connect method
 * allowing the caller to carry on doing things waiting for successful connection or disconnection.
 * @param connect_callback Method to call when connection event occurs
 * @param disconnect_callback Method to call when disconnection event occurs
 * @param username Username to use on connection to broker
 * @param password Password to use on connection to broker
 * @param cafile Path to the CA file to use for TLS connection. Defaults to empty string if no path passed
 * @param key Path to the Key file to use for TLS connection. Defaults to empty string if no path passed
 * @param cert Path to the Certificate file to use for TLS connection. Defaults to empty string if no path passed
 * @throws runtime_error If error in setting TLS options
 * @throws runtime_error If error in connecting to the broker
 **/
void Mqtt::Connect(std::function<void(void)> connect_callback, std::function<void(void)> disconnect_callback, const std::string& username, const std::string& password, const std::string& cafile, const std::string& key, const std::string& cert)
{
    int connection_status;

    connect_callback_ = connect_callback;
    disconnect_callback_ = disconnect_callback;

    mosquitto_loop_start(mosquitto_conn_);

    // CA file mandatory for TLS connection, key and cert are not
    if (!cafile.empty())
    {
        std::cerr << cafile << std::endl;
        int tls_status = mosquitto_tls_set(mosquitto_conn_, cafile.c_str(), NULL,
                                            cert.empty() ? NULL : cert.c_str(),
                                            key.empty() ? NULL : key.c_str(),
                                            NULL);
        if (MOSQ_ERR_SUCCESS != tls_status)
        {
            std::cerr << tls_status << std::endl;
            throw std::runtime_error("Error in setting TLS params");
        }

        int tls_opts_status = mosquitto_tls_opts_set(mosquitto_conn_, 0 /*VERIFY NONE*/,"tlsv1.2",NULL);
        if (MOSQ_ERR_SUCCESS != tls_opts_status)
        {
            std::cerr << tls_opts_status << std::endl;
            throw std::runtime_error("Error in setting TLS options");
        }
    }

    // Set username and password on connection, if there is a set supplied
    if (!username.empty() && !password.empty())
    {
        mosquitto_username_pw_set(mosquitto_conn_, username.c_str(), password.c_str());
    }

    // Connect
    connection_status = mosquitto_connect_async(mosquitto_conn_,broker_addr_.toStdString().c_str(),broker_port_, 10);
    if (MOSQ_ERR_SUCCESS != connection_status)
    {
        throw std::runtime_error("Error in connecting: " + std::to_string(connection_status));
    }
}

/**
 * @brief Disconnect from the broker and stop the mosquitto instance event loop. Al lsubscriptions are
 *      are cleared from internal collection
 * @throws runtime_error If error during disconnection
 * @throws runtime_error If error in stopping mosquitto instance event loop
 **/
void Mqtt::Disconnect()
{
    int connection_status = mosquitto_disconnect(mosquitto_conn_);

    if (MOSQ_ERR_SUCCESS != connection_status)
    {
        throw std::runtime_error("Error in disconnecting");
    }

    int stop_status = mosquitto_loop_stop(mosquitto_conn_, true);
    if (MOSQ_ERR_SUCCESS != stop_status)
    {
        throw std::runtime_error("Error in stopping mosquitto");
    }

    // Remove all Subscriptions
    subscriptions_.clear();
}


/**
 * @brief Destroys the mosquitto instance and cleansup mosquitto
 **/
void Mqtt::Destroy()
{
    mosquitto_destroy(mosquitto_conn_);

    mosquitto_lib_cleanup();

    mosquitto_conn_ = NULL;
}


/**
 * @brief Called when a message is received from the broker. Message data (topic, payload, qos) are retrieved and
 *      passed onto method which then routes the message out to the library user. The timestamp of received message is grabbed here.
 * @param connection Pointer to mosquitto instance
 * @param userdata Pointer to mosquitto userdata
 * @param message Pointer to the received message struct
 **/
void Mqtt::MessageReceivedCallback(struct mosquitto *connection, void *userdata, const struct mosquitto_message *message)
{
    Mqtt* mqtt = reinterpret_cast<Mqtt*>(userdata);

    // Grab the current timestamp, we'll need that later...
    std::chrono::seconds timestamp = std::chrono::seconds(std::time(nullptr));

    // Get the qos
    int qos = message->qos;

    QString payload = QString::fromUtf8(static_cast<const char*>(message->payload));
    mqtt->RouteReceivedMessage(message->topic,payload, timestamp, qos);
}

/**
 * @brief Called when a connection event occurs. Event is passed on for routing to library user
 * @param connection Pointer to mosquitto instance
 * @param userdata Pointer to mosquitto userdata
 * @param conn_code Connection response code
 **/
void Mqtt::ConnectCallback(struct mosquitto *connection, void *userdata, int conn_code)
{
    Mqtt* mqtt = reinterpret_cast<Mqtt*>(userdata);
    mqtt->PassConnectionEvent();
}

/**
 * @brief Called when a disconnection event occurs. Event is passed on for routing to library user
 * @param connection Pointer to mosquitto instance
 * @param userdata Pointer to mosquitto userdata
 * @param conn_code Disconnection response code
 **/
void Mqtt::DisconnectCallback(struct mosquitto *connection, void *userdata, int conn_code)
{
    Mqtt* mqtt = reinterpret_cast<Mqtt*>(userdata);
    mqtt->PassDisconnectionEvent();
}

/**
 * @brief Finds the callback registered for a message subscription which the message topic matches then calls it passing all params on.
 * @param topic The topic of received message
 * @param payload The payload of received message
 * @param timestamp The timestamp of received message
 * @param qos The QoS of received message
 **/
void Mqtt::RouteReceivedMessage(QString topic, QString payload, std::chrono::seconds timestamp, int qos)
{
    bool result;

    for (auto const& x : subscriptions_)
    {
        mosquitto_topic_matches_sub(x.first.toStdString().c_str(),topic.toStdString().c_str(),&result);
        if (result)
        {
            x.second(topic.toStdString(), payload.toStdString(), timestamp, qos);
        }
    }
}

/**
 * @brief Calls the set callback for Connection events
 **/
void Mqtt::PassConnectionEvent()
{
    connect_callback_();
}

/**
 * @brief Calls the set callback for Disconnection events
 **/
void Mqtt::PassDisconnectionEvent()
{
    disconnect_callback_();
}


/**
 * @brief Subscribes to the given topic, and sets callback to be called when a message is received on this subscription
 * @param topic The topic pattern to subscribe to
 * @param callback The method to call when a message is received
 * @param qos The QoS used for subscription
 * @throws runtime_error if already subscribed to given topic
 * @throws runtime_error if subscribe is unsuccessful
 **/
void Mqtt::Subscribe(QString topic, std::function<void(const std::string &, const std::string &, const std::chrono::seconds &, const int &)> callback, int qos)
{

    std::map<QString, std::function<void(const std::string &, const std::string &, const std::chrono::seconds &, const int &)>>::iterator it = subscriptions_.find(topic);
    if (it == subscriptions_.end())
    {
        subscriptions_.insert(std::pair<QString, std::function<void(const std::string &, const std::string &, const std::chrono::seconds &, const int &)>>(topic, callback));
    }
    else
    {
        throw std::runtime_error("Already subscribed to this topic");
    }

    int sub_return_ = mosquitto_subscribe(mosquitto_conn_, NULL, topic.toStdString().c_str(), qos);
    if (MOSQ_ERR_SUCCESS != sub_return_)
    {
        std::cerr << sub_return_ << std::endl;
        throw std::runtime_error("Subscribe failed");
    }
}


/**
 * @brief Unsubscribes from the given topic with broker and removes the topic from insternal subscription collection
 * @param topic The topic to unsubscribe from
 **/
void Mqtt::Unsubscribe(QString topic)
{
    // Remove from the map of subscriptions
    std::map<QString, std::function<void(const std::string &, const std::string &, const std::chrono::seconds &, const int &)>>::iterator it = subscriptions_.find(topic);
    if (it != subscriptions_.end())
    {
        subscriptions_.erase(it);
    }

    mosquitto_unsubscribe(mosquitto_conn_, NULL, topic.toStdString().c_str());
}

/**
 * @brief Publish a message to the broker
 * @param topic The topic to publish message upon
 * @param payload The payload of message to publish
 * @param qos The QoS of the message to publish
 * @param retained Whether to set the message to be retained or not (true = retained)
 **/
void Mqtt::Publish(QString topic, QString payload, int qos, bool retained)
{
    mosquitto_publish(mosquitto_conn_, NULL, topic.toStdString().c_str(), payload.size(), payload.toStdString().c_str(), qos, retained);
}


/**
 * @brief Checks whether given topic matches the subscription given
 * @return true If topic matches subscription
 * @return false If topic does not match subscription
 **/
bool Mqtt::DoesTopicMatchSub(const QString& subscription, const QString& topic)
{
    bool result;

    mosquitto_topic_matches_sub(subscription.toStdString().c_str(),topic.toStdString().c_str(),&result);

    return result;
}