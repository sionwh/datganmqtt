/** @file
 * @brief Header of the BrokerConfig class which is the UI for setting and dispalying connection
 * profile attributes
 *
 * Copyright 2020 Siôn Hughes
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
 * and associated documentation files (the "Software"), to deal in the Software without restriction, 
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, 
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all copies or 
 * substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
 * NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 **/

#ifndef BROKERCONFIG_H
#define BROKERCONFIG_H

#include <QDialog>

namespace Ui {
class BrokerConfig;
}

class BrokerConfig : public QDialog
{
    Q_OBJECT

public:
    explicit BrokerConfig(QWidget *parent = nullptr);
    ~BrokerConfig();
    void SetUIFromConfig(QString path);
    static const std::string CONFIG_PATH;

private slots:
    void on_buttonBox_accepted();
    void on_check_TLS_stateChanged(int state);
    void on_pb_CA_clicked();
    void on_pb_Cert_clicked();
    void on_pb_Key_clicked();

private:
    Ui::BrokerConfig *ui;

    QString LoadFromFile(QString type, QString extension, QString path=std::getenv("HOME"));
    QString LoadFromFile(std::vector<std::pair<QString,QString>> types, QString path=std::getenv("HOME"));
};

#endif // BROKERCONFIG_H
